package repository;

import domain.EnumerationValue;

public interface IEnumerationValueRepository extends IRepository<EnumerationValue> {

	public EnumerationValue withName(String name);
	public EnumerationValue withIntKey(int key, String name);
	public EnumerationValue withStringKey(String key, String name);
	
}
