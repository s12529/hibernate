package repository;

public interface IRepositoryCatalog {

	public IEnumerationValueRepository enumerations();
	public IUserRepository users();
	public void saveChanges();
	
}
