package repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import domain.EntityClass;
import hibernate.HibernateUtil;
import uow.IUnitOfWork;
import uow.IUnitOfWorkRepository;

public abstract class Repository<TEntity extends EntityClass> implements IRepository<TEntity>, IUnitOfWorkRepository {

	protected IUnitOfWork uow;
	protected PreparedStatement selectByID;
	protected PreparedStatement insert;
	protected PreparedStatement delete;
	protected PreparedStatement update;
	protected PreparedStatement selectAll;
	protected IEntityBuilder<TEntity> builder;
	
	protected Session session = HibernateUtil.getSessionFactory().openSession();

	protected SQLQuery selectByIDSql= session.createSQLQuery("SELECT * FROM " + getTableName() + " WHERE id=?");
	protected SQLQuery deleteSql = session.createSQLQuery("DELETE FROM " + " WHERE id=?");
	protected SQLQuery selectAllSql= session.createSQLQuery("SELECT * FROM " + getTableName());

	protected Repository(
			IEntityBuilder<TEntity> builder, IUnitOfWork uow)
	{
		this.uow=uow;
		this.builder=builder;
		

		session.beginTransaction();
		session.save(selectByID);
		session.save(insert);
		session.save(delete);
		session.save(update);
		session.save(selectAll);
		session.getTransaction().commit();
	}
	

	public void add(TEntity entity) {
		uow.markAsNew(entity, this);
	}

	public void modify(TEntity entity) {
		uow.markAsChanged(entity, this);
	}

	public void delete(TEntity entity) {
		uow.markAsDeleted(entity, this);
	}

	public TEntity withId(int id) {
		try {
			selectByID.setInt(1, id);
			ResultSet rs = selectByID.executeQuery();
			while (rs.next()) {
				return builder.build(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	public List<TEntity> allOnPage(PagingInfo page) {
		List<TEntity> result = new ArrayList<TEntity>();

		try {
			ResultSet rs = selectAll.executeQuery();
			while (rs.next()) {
				result.add(builder.build(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return result;
	}

	@SuppressWarnings("unchecked")
	public void persistAdd(EntityClass entity) {
		try {
			setUpInsertQuery((TEntity) entity);
			insert.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public void persistUpdate(EntityClass entity) {

		try {
			setUpInsertQuery((TEntity) entity);
			update.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void persistDelete(EntityClass entity) {
		try {
			delete.setInt(1, entity.getId());
			delete.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	protected abstract void setUpUpdateQuery(TEntity entity) throws SQLException;
	protected abstract void setUpInsertQuery(TEntity entity) throws SQLException;
	protected abstract String getTableName();
	protected abstract String getUpdateQuery();
	protected abstract String getInsertQuery();
}