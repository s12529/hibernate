package repository;

import java.sql.ResultSet;
import java.sql.SQLException;

import domain.EntityClass;

public interface IEntityBuilder<TEntity extends EntityClass> {
	
	public TEntity build(ResultSet rs) throws SQLException;

}
