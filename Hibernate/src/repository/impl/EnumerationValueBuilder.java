package repository.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import domain.EnumerationValue;
import repository.IEntityBuilder;

public class EnumerationValueBuilder implements IEntityBuilder<EnumerationValue>  {

		public EnumerationValue build(ResultSet rs) throws SQLException {
			EnumerationValue enumerationValue = new EnumerationValue();
			enumerationValue.setEnumerationName(rs.getString("enumerationName"));
			enumerationValue.setId(rs.getInt("id"));
			enumerationValue.setIntKey(rs.getInt("intKey"));
			enumerationValue.setStringKey(rs.getString("stringKey"));
			enumerationValue.setValue(rs.getString("value"));
			return enumerationValue;
		}
	
	
	
}
