package repository.impl;

import java.sql.Connection;

import repository.IRepositoryCatalog;
import repository.IUserRepository;
import repository.IEnumerationValueRepository;
import uow.IUnitOfWork;



public class HsqlRepositoryCatalog implements IRepositoryCatalog {
	private IUnitOfWork uow;
	
	public HsqlRepositoryCatalog(Connection connection, IUnitOfWork uow) {
		this.uow = uow;
	}

	
	public void saveChanges(){
		uow.saveChanges();
	}

	public IUserRepository users() {
		return new HsqlUserRepository(new UserBuilder(), uow);
	}

	public IEnumerationValueRepository enumerations() {
		return null;
	}
}