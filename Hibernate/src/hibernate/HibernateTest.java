package hibernate;

import org.hibernate.Session;

import domain.EntityState;
import domain.RolePermissions;
import domain.UserRoles;

public class HibernateTest {

	public static void main(String[] args) {
		
		UserRoles userRoles = new UserRoles(EntityState.NEW, 1, 2);
		RolePermissions rolePermissions = new RolePermissions(EntityState.NEW, 2, 1);
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		session.save(userRoles);
		session.save(rolePermissions);
		session.getTransaction().commit();
		
	}

}
