package uow;

import domain.EntityClass;

public interface IUnitOfWorkRepository {

	public void persistAdd(EntityClass entity);
	public void persistDelete(EntityClass entity);
	public void persistUpdate(EntityClass entity);
	
}
