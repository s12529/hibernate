package uow;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import domain.EntityClass;
import domain.EntityState;

public class HsqlUnitOfWork implements IUnitOfWork {
	
	private Map<EntityClass, IUnitOfWorkRepository> entities;
	private Connection connection;
	
	public HsqlUnitOfWork() {
		entities = new LinkedHashMap<EntityClass, IUnitOfWorkRepository>();
	}
	
	public HsqlUnitOfWork(Connection connection) {
		super();
		this.connection = connection;
		
		try {
			connection.setAutoCommit(false);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void saveChanges() {
		
		for(EntityClass entity: entities.keySet())
		{
			switch(entity.getState())
			{
			case MODIFIED:
				entities.get(entity).persistUpdate(entity);
				break;
			case DELETED:
				entities.get(entity).persistDelete(entity);
				break;
			case NEW:
				entities.get(entity).persistAdd(entity);
				break;
			case UNCHANGED:
				break;
			default:
				break;}
		}
		
		try {
			connection.commit();
			entities.clear();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void undo() {
		entities.clear();
	}

	public void markAsNew(EntityClass entity, IUnitOfWorkRepository repo) {
		entity.setState(EntityState.NEW);
		entities.put(entity, repo);
	}

	public void markAsDeleted(EntityClass entity, IUnitOfWorkRepository repo) {
		entity.setState(EntityState.DELETED);
		entities.put(entity, repo);
	}

	public void markAsChanged(EntityClass entity, IUnitOfWorkRepository repo) {
		entity.setState(EntityState.MODIFIED);
		entities.put(entity, repo);
	}

	public Map<EntityClass, IUnitOfWorkRepository> getEntities() {
		return entities;
	}

	public void setEntities(Map<EntityClass, IUnitOfWorkRepository> entities) {
		this.entities = entities;
	}
	
	

}
