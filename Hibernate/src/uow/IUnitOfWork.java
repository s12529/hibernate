package uow;

import domain.EntityClass;

public interface IUnitOfWork {
	
	public void saveChanges();
	public void undo();
	public void markAsNew(EntityClass entity, IUnitOfWorkRepository repo);
	public void markAsDeleted(EntityClass entity, IUnitOfWorkRepository repo);
	public void markAsChanged(EntityClass entity,IUnitOfWorkRepository repo);

}
