package domain;


import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table (name = "t_u_user")
public class User extends EntityClass {
	
	@Column (name = "login")
	private String login;

	@Column (name = "password")
	private String password;

	@ElementCollection
	private Set<UserRoles> userRoles;

	@ElementCollection
	private Set<RolePermissions> userPermissions;
	
	public User() {
		userRoles = new HashSet<UserRoles>();
		userPermissions = new HashSet<RolePermissions>();
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void addUserRole(UserRoles userRole) {
		this.userRoles.add(userRole);
	}
	
	public void addRolePermission(RolePermissions rolePermission) {
		this.userPermissions.add(rolePermission);
	}

	public Set<UserRoles> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(Set<UserRoles> userRoles) {
		this.userRoles = userRoles;
	}

	public Set<RolePermissions> getUserPermissions() {
		return userPermissions;
	}

	public void setUserPermissions(Set<RolePermissions> userPermissions) {
		this.userPermissions = userPermissions;
	}

	public User(EntityState state, String login, String password) {
		super();
		this.login = login;
		this.password = password;;
	}

	
	
	
	
	
	

}
