package domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table (name = "t_r_permission")
public class RolePermissions extends EntityClass {

	@Column(name = "u_role_id")
	private int roleId;
	@Column(name = "perm_id")
	private int permissionId;
	
	public int getRoleId() {
		return roleId;
	}
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	public int getPermissionId() {
		return permissionId;
	}
	public void setPermissionId(int permissionId) {
		this.permissionId = permissionId;
	}
	
	public RolePermissions(EntityState state, int roleId, int permissionId) {
		super(state);
		this.roleId = roleId;
		this.permissionId = permissionId;
	}
	
	
		
}
