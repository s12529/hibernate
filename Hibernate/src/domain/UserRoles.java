package domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table (name = "t_u_roles")
public class UserRoles extends EntityClass {

	@Column(name = "u_id")
	private int userId;
	@Column(name = "u_role_id")
	private int roleId;
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getRoleId() {
		return roleId;
	}
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	
	public UserRoles(EntityState state, int userId, int roleId) {
		super(state);
		this.userId = userId;
		this.roleId = roleId;
	}
	
	
	
	 
	
	
	
}
