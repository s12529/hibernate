package domain;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name = "t_p_person")
public class Person extends EntityClass {
	
    @Column(name = "first_name")
	private String firstName;
    @Column(name = "surname")
	private String surname;
    @Column(name = "pesel")
	private String pesel;
    @Column(name = "nip")
	private String nip;
    @Column(name = "email")
	private String email;
    @Column(name = "date_of_birth")
	private Calendar dateOfBirth;
    @OneToOne
	private User user;
    @ElementCollection
	private List<Address> address;
    @ElementCollection
	private List<PhoneNumber> phoneNumber;
	
	public Person() {
		address = new ArrayList<Address>();
		phoneNumber = new ArrayList<PhoneNumber>();
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getPesel() {
		return pesel;
	}
	public void setPesel(String pesel) {
		this.pesel = pesel;
	}
	public String getNip() {
		return nip;
	}
	public void setNip(String nip) {
		this.nip = nip;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Calendar getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Calendar dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public List<Address> getAddress() {
		return address;
	}

	public void setAddress(List<Address> address) {
		this.address = address;
	}

	public List<PhoneNumber> getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(List<PhoneNumber> phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Person(EntityState state, String firstName, String surname, String pesel, String nip, String email,
			Calendar dateOfBirth, User user, List<Address> address, List<PhoneNumber> phoneNumber) {
		super(state);
		this.firstName = firstName;
		this.surname = surname;
		this.pesel = pesel;
		this.nip = nip;
		this.email = email;
		this.dateOfBirth = dateOfBirth;
		this.user = user;
		this.address = address;
		this.phoneNumber = phoneNumber;
	}
	
	
	
}
