package domain;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class EntityClass {

	@Id
	@GeneratedValue
	@Column (name = "id")
	private int id;

	@Column (name = "e_state")
	@Enumerated(EnumType.ORDINAL)
	private EntityState state;
	

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public EntityState getState() {
		return state;
	}
	public void setState(EntityState state) {
		this.state = state;
	}
	
	public EntityClass() {}
	
	public EntityClass(EntityState state) {
		this.state = state;
	}
	
	public EntityClass(int id, EntityState state) {
		super();
		this.id = id;
		this.state = state;
	}	
		
	
	
}
