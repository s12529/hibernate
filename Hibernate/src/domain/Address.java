package domain;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Address extends EntityClass {

	@Column(name = "id_country")
	private int countryId;
	@Column(name = "id_region")
	private int regionId;
	@Column(name = "city")
	private String city;
	@Column(name = "street")
	private String street;
	@Column(name = "house_no")
	private String houseNumber;
	@Column(name = "local_no")
	private String localNumber;
	@Column(name = "zip_code")
	private String zipCode;
	@Column(name = "type_id")
	private int typeId;
	
	public int getCountryId() {
		return countryId;
	}



	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}
	
	public int getRegionId() {
		return regionId;
	}

	public void setRegionId(int regionId) {
		this.regionId = regionId;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	public String getLocalNumber() {
		return localNumber;
	}

	public void setLocalNumber(String localNumber) {
		this.localNumber = localNumber;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public int getTypeId() {
		return typeId;
	}

	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}


	public Address(EntityState state, int countryId, int regionId, String city, String street, String houseNumber,
		String localNumber, String zipCode, int typeId) {
	//super(state);
	this.countryId = countryId;
	this.regionId = regionId;
	this.city = city;
	this.street = street;
	this.houseNumber = houseNumber;
	this.localNumber = localNumber;
	this.zipCode = zipCode;
	this.typeId = typeId;
}


 
}