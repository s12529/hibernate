package domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "t_p_phone_number")
public class PhoneNumber extends EntityClass {

	@Column(name = "pre_country")
	private String countryPrefix;
	@Column(name = "pre_city")
	private String cityPrefix;
	@Column(name = "number")
	private String number;
	@Column(name = "type_id")
	private int typeId;
	
	public String getCountryPrefix() {
		return countryPrefix;
	}
	public void setCountryPrefix(String countryPrefix) {
		this.countryPrefix = countryPrefix;
	}
	public String getCityPrefix() {
		return cityPrefix;
	}
	public void setCityPrefix(String cityPrefix) {
		this.cityPrefix = cityPrefix;
	}
	public String getNumer() {
		return number;
	}
	public void setNumer(String numer) {
		this.number = numer;
	}
	public int getTypeId() {
		return typeId;
	}
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

	public PhoneNumber(EntityState state, String countryPrefix, String cityPrefix, String numer, int typeId,
			Person person) {
		super(state);
		this.countryPrefix = countryPrefix;
		this.cityPrefix = cityPrefix;
		this.number = numer;
		this.typeId = typeId;
	}
	

	
	
	
	
}
