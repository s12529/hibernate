package domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "t_p_enumeration_value")
public class EnumerationValue extends EntityClass {

	@Column (name = "key")
	private int intKey;
	@Column (name = "string_key")
	private String stringKey;
	@Column (name = "enum_value")
	private String value;
	@Column (name = "enum_name")
	private String enumerationName;
	
	public int getIntKey() {
		return intKey;
	}
	public void setIntKey(int intKey) {
		this.intKey = intKey;
	}
	public String getStringKey() {
		return stringKey;
	}
	public void setStringKey(String stringKey) {
		this.stringKey = stringKey;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getEnumerationName() {
		return enumerationName;
	}
	public void setEnumerationName(String enumerationName) {
		this.enumerationName = enumerationName;
	}
	public EnumerationValue(EntityState state, int intKey, String stringKey, String value,
			String enumerationName) {
		super(state);
		this.intKey = intKey;
		this.stringKey = stringKey;
		this.value = value;
		this.enumerationName = enumerationName;
	}
	
	public EnumerationValue() {}
	
}
