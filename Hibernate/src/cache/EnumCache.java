package cache;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import domain.EntityState;
import hibernate.HibernateUtil;

public class EnumCache<T> {
	
	private Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	private Transaction tx = null;
	private Map<Integer, EntityState> items = new HashMap<>();
	private static EnumCache instance;
	private Class<T> entityClass;
	
	
	private EnumCache() {}
	
	public static EnumCache getInstance() {
		if (instance == null) {
			instance = new EnumCache();
		}
		return instance;
	}
	
	public void clear() {
		items.clear();
	}
	
	public void add(Integer key){
		 try {
			  tx = session.beginTransaction();       
			  items.put(key, (EntityState) session.get(entityClass, 1));
			  tx.commit();
		 }catch (HibernateException ex) {
			 
			 Logger.getLogger("con").info("Exception: " + ex.getMessage());
		 }
		
	}
	
	public void get(Integer key){
		System.out.println(items.get(key));
	}
	
	public void refreshCache(Map<Integer, EntityState> values){
		clear();
		this.items = values;
	}
	

}
